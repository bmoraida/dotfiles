# -*- coding: utf-8 -*-
import os
import socket
import subprocess
from typing import List  # noqa: F401
from libqtile import qtile
from libqtile.config import Click, Drag, Group, Key, Screen, KeyChord
from libqtile.config import ScratchPad, DropDown, Match

from libqtile.command import lazy
from libqtile import bar, layout, widget, hook

mod = "mod4"
alt = "mod1"
terminal = "alacritty"

keys = [
    ### The essentials
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key(
        [mod, "shift"],
        "Return",
        lazy.spawn("dmenu_run -p 'Run: '"),
        desc="Run Launcher",
    ),
    Key(
        [mod],
        "r",
        lazy.spawn(
            'rofi -show-icons -show drun -display-drun "Run " -drun-display-format "{name}"'
        ),
        desc="Run Launcher",
    ),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown qtile"),
    Key([mod], "x", lazy.window.kill(), desc="Kill focused window"),
    # System keys
    Key(
        [],
        "XF86MonBrightnessUp",
        lazy.spawn("xbacklight -inc 5"),
        desc="Brighter monitor",
    ),
    Key(
        [],
        "XF86MonBrightnessDown",
        lazy.spawn("xbacklight -dec 5"),
        desc="Brighter monitor",
    ),
    Key(
        [mod, "shift"],
        "l",
        lazy.spawn("slock"),
        desc="Locks the screen",
    ),
    Key(
        [alt, "control"],
        "Delete",
        lazy.spawn("sysact"),
        desc="Dmenu list for lock/suspend/logoff action",
    ),
    Key([mod], "d", lazy.spawn("emacsclient -c"), desc="launch emacs"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down(), desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(), desc="Move focus up in stack pane"),
    # Move windows up or down in current stack
    Key(
        [mod, "shift"],
        "k",
        lazy.layout.shuffle_down(),
        desc="Move windows down in current stack",
    ),
    Key(
        [mod, "shift"],
        "j",
        lazy.layout.shuffle_up(),
        desc="Move windows up in current stack",
    ),
    # More window commands
    Key(
        [mod],
        "l",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc="Expand window (MonadTall), increase number in master pane (Tile)",
    ),
    Key(
        [mod],
        "h",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc="Shrink window (MonadTall), decrease number in master pane (Tile)",
    ),
    Key([mod], "n", lazy.layout.normalize(), desc="normalize window size ratios"),
    Key(
        [mod],
        "m",
        lazy.layout.maximize(),
        desc="toggle window between minimum and maximum sizes",
    ),
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc="toggle fullscreen"),
    Key([mod, "shift"], "f", lazy.window.toggle_floating(), desc="toggle floating"),
    Key([mod], "b", lazy.hide_show_bar("all"), desc="Toggle bar visibility"),
    ### Stack controls
    Key(
        [mod, "shift"],
        "Tab",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc="Switch which side main pane occupies (XmonadTall)",
    ),
    Key(
        [mod],
        "space",
        lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack",
    ),
    Key(
        [mod, "shift"],
        "space",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    ### Dmenu/terminal scripts launched with ALT + CTRL + KEY
    Key(
        [alt, "control"],
        "e",
        lazy.spawn("dmenu-edit-configs"),
        desc="Dmenu script for editing config files",
    ),
    Key(
        [alt, "control"],
        "r",
        lazy.spawn("workrefs"),
        desc="Dmenu script for opening work documents",
    ),
    ### Switch focus of monitors
    Key([mod], "period", lazy.next_screen(), desc="Move focus to next monitor"),
    Key([mod], "comma", lazy.prev_screen(), desc="Move focus to prev monitor"),
    # Programs spawned with SUP and ALT
    Key(
        [mod, alt],
        "b",
        lazy.spawn("brave --restore-last-session"),
        desc="Launch Brave with previous session",
    ),
    Key([mod, alt], "c", lazy.spawn("chromium"), desc="Launch Chromium"),
    Key([mod, alt], "t", lazy.spawn("teams"), desc="Launches teams"),
    Key(
        [mod, alt],
        "l",
        lazy.spawn(terminal + " -e calcurse"),
        desc="Launch calcurse",
    ),
    Key([mod], "e", lazy.spawn(terminal + " -e lf"), desc="File Manager"),
    Key(
        [mod, alt],
        "y",
        lazy.spawn(terminal + " -e youtube-viewer"),
        desc="youtube-viewer",
    ),
    # F keys
    Key(
        [mod],
        "F1",
        lazy.spawn("/home/brenton/.screenlayout/onescreen.sh"),
        desc="set back to one screen",
    ),
    Key(
        [],
        "F12",
        lazy.group["scratchpad"].dropdown_toggle("term"),
        desc="Dropdown term",
    ),
    Key([], "F11", lazy.group["scratchpad"].dropdown_toggle("weather"), desc="weather"),
    Key(
        [],
        "F10",
        lazy.group["scratchpad"].dropdown_toggle("yt-dropdown"),
        desc="yt-viewer dropdown",
    ),
    Key(
        [],
        "F9",
        lazy.group["scratchpad"].dropdown_toggle("ncmpcpp"),
        desc="Audio Player",
    ),
]


groups = [Group(i) for i in "uiop7890"]


for i in groups:
    keys.extend(
        [
            # Mod4 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod4 + shift + letter of group = switch to & move focused window
            # to group, switch_group=True after name to switch there
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #     desc="move focused window to group {}".format(i.name)),
        ]
    )

groups.append(
    ScratchPad(
        "scratchpad",
        [
            DropDown(
                "term",
                terminal,
                opacity=1,
                height=0.55,
                width=0.80,
            ),
            DropDown(
                "weather",
                "alacritty -e weath",
                opacity=1,
                height=0.95,
                width=0.80,
                x=0.1,
            ),
            DropDown(
                "yt-dropdown",
                terminal + " -e youtube-viewer",
                opacity=1,
                height=0.8,
                width=0.5,
                x=0.5,
            ),
            DropDown(
                "ncmpcpp", terminal + " -e ncmpcpp", opacity=1, height=0.5, width=0.6
            ),
        ],
    ),
)

layout_theme = {
    "border_width": 2,
    "margin": 6,
    "border_focus": "ff5555",
    "border_normal": "1d2330",
}

layouts = [
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    # layout.Stack(num_stacks=2, **layout_theme),
    # layout.Tile(**layout_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Bsp(),
    # layout.Columns(),
    # layout.Matrix(),
    layout.MonadWide(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

colors = [
    ["#282c34", "#282c34"],  # panel background
    ["#434758", "#434758"],  # background for current screen tab
    ["#eeffff", "#eeffff"],  # font color for group names
    ["#ff5555", "#ff5555"],  # border line color for current tab
    ["#09f9f1", "#09f9f1"],  # widget 1
    ["#2596ed", "#2596ed"],
]

widget_defaults = dict(
    font="Mononoki Nerd Font", fontsize=16, padding=2, background=colors[2]
)
extension_defaults = widget_defaults.copy()


def init_widgets_list():
    widgets_list = [
        widget.Sep(linewidth=0, padding=6, foreground=colors[2], background=colors[0]),
        widget.GroupBox(
            disable_drag=True,
            hide_unused=True,
            margin_y=3,
            margin_x=0,
            padding_y=5,
            padding_x=3,
            borderwidth=3,
            active=colors[2],
            inactive=colors[2],
            rounded=False,
            highlight_color=colors[1],
            highlight_method="line",
            this_current_screen_border=colors[3],
            this_screen_border=colors[4],
            other_current_screen_border=colors[0],
            other_screen_border=colors[0],
            foreground=colors[2],
            background=colors[0],
        ),
        widget.Sep(linewidth=0, padding=40, foreground=colors[2], background=colors[0]),
        widget.WindowName(
            foreground=colors[4],
            background=colors[0],
            padding=0,
            width=600,
        ),
        widget.TaskList(background=colors[0], max_title_width=400, padding=2),
        widget.WidgetBox(
            widgets=[
                widget.Memory(
                    format="{MemUsed: .0f}{mm}/{MemTotal: .0f}{mm}",
                    background=colors[0],
                    foreground=colors[5],
                    padding=5,
                    mouse_callbacks={
                        "Button1": lambda: qtile.cmd_spawn(terminal + " -e ytop")
                    },
                    measure_mem="G",
                ),
                widget.Battery(
                    padding=1, background=colors[0], format=" {char} {percent:2.0%}"
                ),
                widget.TextBox(
                    text=" ☀️", background=colors[0], padding=5, fontsize=15
                ),
                widget.Backlight(
                    backlight_name="intel_backlight", background=colors[0], padding=5
                ),
            ],
            background=colors[0],
        ),
        widget.Sep(linewidth=0, padding=1, foreground=colors[0], background=colors[0]),
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            foreground=colors[0],
            background=colors[0],
            padding=0,
            scale=0.7,
        ),
        widget.CurrentLayout(foreground=colors[2], background=colors[0], padding=5),
        widget.Clock(
            padding=5,
            foreground=colors[5],
            background=colors[0],
            format="%a %b %d [%H:%M]",
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(terminal + " -e calcurse")
            },
        ),
        widget.Sep(linewidth=0, padding=5, foreground=colors[0], background=colors[0]),
        widget.Systray(icon_size=22, background=colors[0], padding=10),
        widget.Sep(linewidth=0, padding=10, foreground=colors[0], background=colors[0]),
        widget.CheckUpdates(
            distro="Arch_yay",
            background=colors[0],
            mouse_callbacks={
                "Button1": lambda: qtile.cmd_spawn(terminal + " -e yay -Syu")
            },
        ),
    ]
    return widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    # Slicing removes unwanted widgets on Monitors 1,3
    return widgets_screen1


def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    # Monitor 2 will display all widgets in widgets_list
    return widgets_screen2


def init_screens():
    return [
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=0.9, size=30)),
        Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=0.9, size=30)),
        Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=0.9, size=30)),
    ]


if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()


def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)


def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)


def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)


# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"

# Needed for some Java programs
wmname = "LG3D"

floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        # default_float_rules include: utility, notification, toolbar, splash, dialog,
        # file_progress, confirm, download and error.
        *layout.Floating.default_float_rules,
        Match(wm_class="pinentry-gtk-2"),  # GPG key password entry
    ]
)

##### STARTUP PROGRAMS #####


@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/autostart.sh"])


##### RESTART ON SCREEN CHANGE #####


@hook.subscribe.screen_change
def restart_on_randr(qtile, ev):
    qtile.cmd_restart()
