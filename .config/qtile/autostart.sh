#!/bin/sh

# This file runs when a DM logs you into a graphical session.
# If you use startx/xinit this file will also be sourced.

numlockx &
nitrogen --restore &			# set the background with nitrogen
#xrdb ${XDG_CONFIG_HOME:-$HOME/.config}/x11/xresources &	# Uncomment to use Xresources colors/settings on startup
xrdb -merge ~/.Xresources
pa-applet &
nm-applet &
mpd &
picom &		# xcompmgr for transparency
xset r rate 300 50 &	# Speed xrate up
unclutter &		# Remove mouse when idle
libinput-gestures & # Allows touchpad gestures (mainly 3 finger 'back' in the browser)
remaps & # script to set caps to escape when tapped and super when held
dunst &			# dunst for notifications
